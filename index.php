<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Cette page sert à la présentation du projet. Il n'y a donc pas de script, juste des inclusions.
$titrePage = 'Accueil';
?>
<!doctype HTML>
<html>
    <?php include('src/php/head.php'); ?>
    <body>
        <div class="container">
            <?php include('src/php/header.php'); ?>
            <?php include('src/php/main_nav.php'); ?>
            <h2 class="row">
                <div class="col-sm-12">
                    <?php echo $titrePage ?>
                </div>
            </h2>
            <section class="row">
                <div class="col-sm-12">
                    <h3>
                        Bienvenue sur le site du projet 4 de C2i1 !
                    </h3>
                    <p>
                        Texte de présentation...
                    </p>
                </div>
            </section>
            <?php include('src/php/footer.php'); ?>
        </div>
    </body>
</html>
