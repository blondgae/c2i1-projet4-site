###Ajout et modification de QCM
Les fichiers contenant les QCM peuvent porter n'importe quel nom, mais ils doivent obligatoirement se terminer par l'extension `.qcm`.

Dans le fichier, l'information est gérée ainsi :
- Ligne 1 : Le titre du QCM
- Ligne 2 : Le descriptif du QCM

Pour chacunes des lignes suivantes on a :
- Ligne commançant par `*` : énoncé de la question
- Ligne commançant par `+` : réponse correcte
- Ligne commançant par `-` : réponse fausse
