<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Page affichant le TP, rien de compliqué

$titrePage = 'TP sur le domaine D2.1';
?>

<!DOCTYPE HTML>
<html>
    <?php include ('src/php/head.php') ?>
    <body>
        <div class="container">
        <?php include ('src/php/header.php') ?>
        <?php include ('src/php/main_nav.php') ?>
        <h2><?php echo $titrePage ?></h2>
        <section>
            <h3>
                Maîtriser son identité numérique privée, institutionnelle
                et professionnelle
            </h3>
            <article>
                <p>
                    1) Soit les mots-de-passe suivants :
                    <ul>
                        <li>Lejourh</li>
                        <li>IAMaMAN</li>
                        <li>MPsoa12</li>
                        <li>PaulFR@N&OIS</li>
                    </ul>
                    Ordonnez du mot-de-passe que vous estimez le plus sûr à
                    celui que vous estimez le moins sûr.
                </p>
            </article>
            <article>
                <p>
                    2) Soit un service d'internet quelconque, identifiez-vous
                    sur un service de votre choix (n'oubliez pas de faire des
                    copies d'écran montrant le processus d'identification et
                    vos coordonnées qui s'affichent à l'écran après
                    l'identification).
                </p>
            </article>
            <article>
                <p>
                    3) Donner au moins  6 exemples de services d'identification
                    privés, professionnels et anonymes en complétant le tableau
                    suivant :
                </p>
                <table>
                    <tr>
                        <th></th>
                        <th>Privés</th>
                        <th>Professionnels</th>
                        <th>Anonymes</th>
                    </tr>
                    <tr>
                        <th>1</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>2</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>3</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>4</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>5</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>6</th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </article>
            <article>
                <p>
                    4.1) Sur le navigateur installé sur votre ordinateur,
                    montrer comment accéder à :
                    <ol>
                        <li>L'historique ;</li>
                        <li>Le cache du navigateur ;</li>
                        <li>Les cookies ;</li>
                        <li>Les fichiers téléchargés.</li>
                    </ol>
                    Ne pas oublier de prendre plusieurs copies d'écran
                    montrant ce processus.
                </p>
                <p>
                    4.2) Quel type de trace ces quatre point constituent?
                </p>
            </article>
            <article>
                <p>
                    5) À l'aide d'un compte sur un réseau social quelconque que
                    vous possédez, consultez vos paramètres de confidentialité.
                    Qui peut voir votre photo de profil ou votre nom, vos
                    photos et vos coordonnées (utilisez des copies d'écran pour
                    répondre à cette question) ?
                </p>
            </article>
            <article>
                <p>
                    6) Consultez les conditions d'utilisation des données que
                    vous y publiez et relevez les trois points qui vous
                    semblent les plus importants(utilisez des copies d'écran
                    pour répondre à cette question).
                </p>
            </article>
            <article>
                <p>
                    7) Qu'est-ce que la e-réputation? Spécifiez des bonnes
                    pratiques pour avoir une bonne e-réputation.
                </p>
            </article>
        </section>
        <?php include ('src/php/footer.php') ?>
        </div>
    </body>
</html>