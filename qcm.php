<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Page traitant les QCM
$titrePage = 'QCM';

// On récupère la liste des QCM
$files = scandir('./qcm/', SCANDIR_SORT_ASCENDING);
$qcmFiles = null;
foreach ($files as $fileName) {
    $extensions = explode('.', $fileName);
    if ($extensions[count($extensions) - 1] == 'qcm') {
        $qcmFiles[] = $fileName;
    }
}

include('src/php/liste_qcm.php');
include('src/php/formulaire_qcm.php');
?>

<!doctype HTML>
<html>
    <?php include('src/php/head.php'); ?>
    <body>
        <?php include('src/php/header.php'); ?>
        <?php include('src/php/main_nav.php'); ?>
        <h2>
            <?php echo $titrePage ?>
        </h2>
        <?php
            if (!isset($_GET['qcm']) || empty(htmlspecialchars($_GET['qcm']))) {
                ?>
                    <section>
                        <h3>
                            QCM disponibles :
                        </h3>
                        <?php echo generateQcmList($qcmFiles) ?>
                    </section>
                <?php
            }
            else {
                ?>
                    <section>
                        <?php echo generateQcmForm($_GET['qcm']) ?>
                    </section>
                <?php
            }
        ?>
        <?php include('src/php/footer.php'); ?>
    </body>
</html>
