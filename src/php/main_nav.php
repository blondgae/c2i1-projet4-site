<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Page générant les menus

?>
<nav>
    <ul>
        <li>
            <a href="index.php">Page d'accueil</a>
        </li>
        <li>
            <a href="qcm.php">QCM</a>
        </li>
        <li>
            <a href="tp.php">TP</a>
        </li>
    </ul>
</nav>
