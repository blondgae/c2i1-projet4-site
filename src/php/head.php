<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

 // Page générant la partie <head> en html. la variable titrePage doit être définie !
?>
<head>
    <meta http-equiv="Content-Type" content="type=Text/Html;charset=utf8">
    <title>
        <?php echo 'Projet 4 de C2i1 - ' . $titrePage ?>
    </title>
    <script src="src/js/jquery-3.2.1.min.js"></script>
    <script src="src/js/bootstrap.js"></script>
    <script src="src/css/cyborg.min.css"></script>
    <link rel="stylesheet" href="src/css/style.css">
</head>
