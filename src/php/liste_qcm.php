<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Exporte une fonction permettant de générer la liste des QCM disponibles.


function generateQcmList($qcmFiles) {
    $templateQcmItemList = '<li><a href="qcm.php?qcm=%s">%s</a></li>';
    $finalHtmlCode = '<ul>';    
    foreach ($qcmFiles as $fileName) {
        $qcm = fopen ('qcm/'.$fileName, 'r');
        $titre = fgets($qcm); // On récupère la première ligne = le titre
        $finalHtmlCode .= sprintf($templateQcmItemList, $fileName, $titre);
    }
    $finalHtmlCode .= '</ul>';
    return $finalHtmlCode;
}
