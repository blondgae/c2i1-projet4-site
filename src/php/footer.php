<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights : CC-BY-NC
 */
?>
<footer class="page-header">
    <p>
        Site réalisé dans le cadre du cours de C2i1. Auteurs : Gaëtan Blond, Florent Delarue, Pompilio Manuel. Groupe de travail 21.
    </p>
</footer>
