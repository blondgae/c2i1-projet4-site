<?php
/**
 * Created by Gaëtan Blond
 *
 * Copyrights: CC-BY-NC
 */

// Génère le formulaire du QCM correspondant et compte les points si celui-ci a été validé.

define("CH_NEW_QUESTION", '*');
define("CH_GOOD_ANSWER", '+');
define("CH_WRONG_ANSWER", '-');

define("VALIDATE_BUTTON", 'validate');
define("VALIDATE_TEXT", 'Valider');

function generateQuestion($sentence, $answers,$isFormValidated, $idQuestion, &$score) {
    $wellAnswered = true;
    
    $formPart = '<p>';
    $formPart .= sprintf('%d) %s', $idQuestion+1, $sentence);
    $answerLineType = '<br/><label><input type="checkbox" name="%s" value="yes"/>%s</label>';
    $validatedAnswerLineType = '<br/><label class="%s"><input type="checkbox" disabled %s/>%s</label>';

    foreach ($answers as $idAnswer => $answer) {
        $checkboxName = sprintf('qcm[%d][%d]', $idQuestion, $idAnswer);
        if (!$isFormValidated)
            $formPart .= sprintf($answerLineType, $checkboxName, ltrim($answer, '+-'));
        else{
            $typeAnswer = '';
            $checked = isset($_POST['qcm'][$idQuestion][$idAnswer]) && ($_POST['qcm'][$idQuestion][$idAnswer] == 'yes');
            if ($checked xor ($answer[0] == '+')) {
                $typeAnswer = 'wrongAnswer';
                $wellAnswered = false;
            }
            else
                $typeAnswer = 'goodAnswer';
            $formPart .= sprintf($validatedAnswerLineType, $typeAnswer, $checked?'CHECKED':'', ltrim($answer, '-+'));
        }
    }
    
    $formPart .= '</p>';
    if ($wellAnswered)
        $score ++;
    
    return $formPart;
}

function generateQcmForm ($fileName) {
    $isFormValidated = isset($_POST[VALIDATE_BUTTON]) && $_POST[VALIDATE_BUTTON] == VALIDATE_TEXT;
    $nbQuestions = 0;
    $score = 0;
    
    $finalForm = '';
    
    $file = fopen('./qcm/' . $fileName, 'r');
    $finalForm .= '<h3>' . fgets($file) . '</h3>';
    $finalForm .= 'Description : ' . fgets($file);
    $finalForm .= '<form method="POST" action="qcm.php?qcm=' . $fileName . '">';
    
    $sentence = '';
    $answers = null;
    while ($line = fgets($file)) { // On sort de la boucle si la lecture a échoué
        switch ($line[0]) {
            case CH_NEW_QUESTION:
                if ($sentence != ''){
                    $finalForm .= generateQuestion($sentence, $answers, $isFormValidated, $nbQuestions, $score);
                    $nbQuestions++;
                }
                $sentence = ltrim($line, '*');
                $answers = null;
                break;
            case CH_GOOD_ANSWER:
            case CH_WRONG_ANSWER:
                $answers[] = $line;
                break;
            default:
                throw new Exception('Error: file not well constructed!');
        }
    }
    if ($sentence != '')
        $finalForm .= generateQuestion($sentence, $answers, $isFormValidated, $nbQuestions, $score);
    
    fclose($file);

    if ($isFormValidated){
        $finalForm .= '</form>';
        $finalForm .= sprintf('<p>Ton score : %d/%d </p>', $score, $nbQuestions+1);
    }
    else {
        $finalForm .= '<input type="submit" name="' . VALIDATE_BUTTON . '" value="' . VALIDATE_TEXT . '"/>';
        $finalForm .= '</form>';
    }
    
    return $finalForm;
}
